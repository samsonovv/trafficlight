import Vue from 'vue'
import VueRouter from 'vue-router'
import TrafficLight from '@/components/TrafficLight'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/yellow'
    },
    {
      name: 'Red signal',
      path: '/red',
      props: {
        color: 'red',
        activeTime: 10
      },
      component: TrafficLight
    },
    {
      name: 'Yellow signal',
      path: '/yellow',
      props: {
        color: 'yellow',
        activeTime: 3
      },
      component: TrafficLight
    },
    {
      name: 'Green signal',
      path: '/green',
      props: {
        color: 'green',
        activeTime: 15
      },
      component: TrafficLight
    }
  ]
})
